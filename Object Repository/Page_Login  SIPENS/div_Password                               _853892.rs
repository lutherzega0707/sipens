<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Password                               _853892</name>
   <tag></tag>
   <elementGuidId>ab860b33-2ee1-4512-8af3-61cea4c4ad54</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.d-block</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='app']/section/div/div/div/div/div[2]/form/div[2]/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>38d5d042-500a-4be9-8860-562e45646f39</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>d-block</value>
      <webElementGuid>17d85d9d-3403-4e9a-900f-cf2914d28856</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                        Password
                                                                                    
                                                
                                                    Forgot Password?
                                                
                                            
                                                                            </value>
      <webElementGuid>dd97c593-5cf3-4bda-96ac-adf8dac57aec</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/section[@class=&quot;section&quot;]/div[@class=&quot;container mt-5&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 col-lg-6 offset-lg-3 col-xl-4 offset-xl-4&quot;]/div[@class=&quot;card card-primary&quot;]/div[@class=&quot;card-body&quot;]/form[@class=&quot;needs-validation&quot;]/div[@class=&quot;form-group&quot;]/div[@class=&quot;d-block&quot;]</value>
      <webElementGuid>fa95cb94-651b-420d-a628-85200b0eecc2</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/section/div/div/div/div/div[2]/form/div[2]/div</value>
      <webElementGuid>b6f628f7-8d52-409a-8d8f-bb7053eb3e21</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Please fill in your NIP/NISN'])[1]/following::div[2]</value>
      <webElementGuid>7ab4a23e-c0d8-4922-8697-e9430cd44c0e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Username'])[1]/following::div[3]</value>
      <webElementGuid>6e634135-d56b-4b2a-b974-46453f242fb7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div</value>
      <webElementGuid>efca7ce0-564a-4566-9b7b-417eb6fdaa69</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                                        Password
                                                                                    
                                                
                                                    Forgot Password?
                                                
                                            
                                                                            ' or . = '
                                        Password
                                                                                    
                                                
                                                    Forgot Password?
                                                
                                            
                                                                            ')]</value>
      <webElementGuid>7e8f0c0b-8dc8-42ea-99b4-d3731bdd0f56</webElementGuid>
   </webElementXpaths>
</WebElementEntity>

<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Plotting Guru Wali</name>
   <tag></tag>
   <elementGuidId>a9080e74-e81e-453f-9727-9a078704a428</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//aside[@id='sidebar-wrapper']/ul/li[13]/a/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>ff849b63-e15c-4cc9-a21b-60fc9532d943</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Plotting Guru Wali</value>
      <webElementGuid>d0016c91-1ddb-4961-891d-8b3f3466d0b3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;sidebar-wrapper&quot;)/ul[@class=&quot;sidebar-menu&quot;]/li[13]/a[@class=&quot;nav-link&quot;]/span[1]</value>
      <webElementGuid>a1bb6c9d-4fc4-4205-82a6-6c9ad9bc8aa9</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//aside[@id='sidebar-wrapper']/ul/li[13]/a/span</value>
      <webElementGuid>3fc4b773-411d-4c31-9143-1d731fd01f21</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Plotting'])[1]/following::span[1]</value>
      <webElementGuid>28b5ff29-c377-4042-865f-baf5dfb3cfbf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Data Siswa'])[1]/following::span[1]</value>
      <webElementGuid>ff8653de-2c79-4e35-ab88-271bc562e0ed</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Plotting Guru Mapel'])[1]/preceding::span[1]</value>
      <webElementGuid>b654d6f0-9b51-4271-bc9f-3403836ce5f0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Plotting Siswa'])[1]/preceding::span[2]</value>
      <webElementGuid>67ceec75-d251-4adb-a601-83e6b1770600</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Plotting Guru Wali']/parent::*</value>
      <webElementGuid>822f15e7-4d6f-4d9b-a182-2e65d3473df4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[13]/a/span</value>
      <webElementGuid>ca3376a2-3509-4707-9c47-db26cd6d2529</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Plotting Guru Wali' or . = 'Plotting Guru Wali')]</value>
      <webElementGuid>93c65164-6b91-42bc-a70c-88001c9a2914</webElementGuid>
   </webElementXpaths>
</WebElementEntity>

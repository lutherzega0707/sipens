<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Tambah Data Siswa</name>
   <tag></tag>
   <elementGuidId>fed9a610-01a7-4586-92c2-dcd591f9b48e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='app']/div/div[3]/section[2]/div/div[2]/a</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>a.btn.btn-success</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>6d9b034a-9edd-47b2-8e08-d71f5730fd29</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-success</value>
      <webElementGuid>3ea4c560-6d96-4f0c-8e68-290df4941b26</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>https://sipens.com/admin/create-siswa</value>
      <webElementGuid>6063ef05-7031-4794-baba-f7ce84bd89d3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                        
                        Tambah Data Siswa
                    </value>
      <webElementGuid>0e8f0aa1-e674-47b5-ab6f-b5d43ba55e04</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;main-wrapper main-wrapper-1&quot;]/div[@class=&quot;main-content&quot;]/section[2]/div[@class=&quot;card col-lg-12 col-md-12&quot;]/div[@class=&quot;card-body p-0 pt-2 pb-4&quot;]/a[@class=&quot;btn btn-success&quot;]</value>
      <webElementGuid>01863cf7-18a1-4b4f-b166-035d0bcc4a36</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/div[3]/section[2]/div/div[2]/a</value>
      <webElementGuid>4c138601-b923-4212-83e5-184e6c29c251</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Tambah Siswa'])[1]/following::a[1]</value>
      <webElementGuid>d2bc7de2-ceab-4ace-a228-a2ac9ae3126a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Informasi Pengguna'])[1]/following::a[1]</value>
      <webElementGuid>55ccce50-34a8-4d08-88b8-504953079ff8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Tambah Data Siswa']/parent::*</value>
      <webElementGuid>db91f5f7-0f9f-4cbb-8bcc-3489672165b4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, 'https://sipens.com/admin/create-siswa')]</value>
      <webElementGuid>5c936e77-a7f1-4ed8-8246-7a1562b92173</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div[2]/a</value>
      <webElementGuid>987f8a6a-8e2b-4e78-b09c-7f55bda4b349</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'https://sipens.com/admin/create-siswa' and (text() = '
                        
                        Tambah Data Siswa
                    ' or . = '
                        
                        Tambah Data Siswa
                    ')]</value>
      <webElementGuid>58465e78-f2db-428e-abb2-742035e0cc66</webElementGuid>
   </webElementXpaths>
</WebElementEntity>

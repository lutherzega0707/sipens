<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Import Data</name>
   <tag></tag>
   <elementGuidId>f0575a2f-28f2-4bfc-ae5d-58766fd4ad97</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//button[@id='modalButton']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#modalButton</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>4cc4c768-3dd4-40fb-b2d9-f09434b09987</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>c9a5b229-957a-4696-946c-f2d83babde97</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-primary</value>
      <webElementGuid>55ffc3ad-126c-49ec-a885-2f0eb373810f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>modalButton</value>
      <webElementGuid>c2e5f164-b203-4a18-a32d-c956805bf2d2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                        
                        Import Data
                    </value>
      <webElementGuid>5c0597fc-7e85-4044-8891-e4d5ae4ef656</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;modalButton&quot;)</value>
      <webElementGuid>8065f8b3-772b-4c33-8194-f96467c228db</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@id='modalButton']</value>
      <webElementGuid>d06b5dc1-51ac-4cf1-917b-93f296345b8a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/div[3]/section[2]/div/div[2]/button</value>
      <webElementGuid>65abde01-fb29-44eb-9ac7-25fa776785d0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Tambah Siswa'])[1]/following::button[1]</value>
      <webElementGuid>4b4a8de9-d475-4e3b-b505-db64431f35bf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Data Siswa'])[3]/preceding::button[1]</value>
      <webElementGuid>01442836-d40b-4d07-83b4-14837e876889</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Import Data']/parent::*</value>
      <webElementGuid>bf8a745a-b691-4fa0-b3c0-749849a37c7d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//button</value>
      <webElementGuid>c6dec64b-8d89-4650-b1c9-0fbe4a411e10</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'button' and @id = 'modalButton' and (text() = '
                        
                        Import Data
                    ' or . = '
                        
                        Import Data
                    ')]</value>
      <webElementGuid>44b07f32-2b74-4ba1-82fc-7503d41ecc76</webElementGuid>
   </webElementXpaths>
</WebElementEntity>

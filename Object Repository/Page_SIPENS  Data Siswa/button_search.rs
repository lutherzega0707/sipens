<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_search</name>
   <tag></tag>
   <elementGuidId>8e4bb6a6-609f-46b9-9969-6127dabdcf90</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='app']/div/div[3]/section[3]/div/div/div/div[2]/div/div[2]/form/div/button</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.mb-3 > button.btn.btn-primary</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>c4db56a1-c4af-4031-91d2-a148e8e00dc7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-primary</value>
      <webElementGuid>4809d408-f304-4b1f-a09b-ee829252f3ec</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                
                                                search
                                            </value>
      <webElementGuid>9db3070a-598d-465d-b240-af6db9db5196</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;main-wrapper main-wrapper-1&quot;]/div[@class=&quot;main-content&quot;]/section[3]/div[@class=&quot;row&quot;]/div[@class=&quot;col-12&quot;]/div[@class=&quot;card&quot;]/div[@class=&quot;card-body&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-4&quot;]/form[1]/div[@class=&quot;mb-3&quot;]/button[@class=&quot;btn btn-primary&quot;]</value>
      <webElementGuid>b94ebb62-f6a5-4bc6-a873-7175fe390c53</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/div[3]/section[3]/div/div/div/div[2]/div/div[2]/form/div/button</value>
      <webElementGuid>49902202-2a1b-43b3-af3d-0cb9028960c9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Data Siswa'])[3]/following::button[2]</value>
      <webElementGuid>e09564b8-2589-4891-8076-e9c77d387864</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='No'])[1]/preceding::button[1]</value>
      <webElementGuid>ee881b83-9d53-4258-8d9d-c8ed738fe494</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='NISN'])[1]/preceding::button[1]</value>
      <webElementGuid>8fafd1fc-bcb2-4872-a46b-79bb8e563c99</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='search']/parent::*</value>
      <webElementGuid>49f2c228-ec3d-4799-b965-50cbe88a0f59</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/form/div/button</value>
      <webElementGuid>db54a292-0b91-4f14-bbe5-0a88a6ff1001</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[(text() = '
                                                
                                                search
                                            ' or . = '
                                                
                                                search
                                            ')]</value>
      <webElementGuid>2fc5404e-4b3f-4d5c-8f32-1c6400d7f43a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>

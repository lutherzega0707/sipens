<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_2023</name>
   <tag></tag>
   <elementGuidId>fb541468-0432-42a2-82b7-8d2a6691344d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//select[@name='angkatan']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>select[name=&quot;angkatan&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>50c4fa87-8011-489f-8e5d-569dbb2920eb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control</value>
      <webElementGuid>1a223cbf-7a4d-4d61-a42d-4da5d68a9793</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>angkatan</value>
      <webElementGuid>88d1dc38-f645-4f1e-bdac-f473bba3d74f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                        
                                                                                    2023
                                                                            </value>
      <webElementGuid>6b190c16-0e7c-4216-a604-bc0d9fba1643</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;main-wrapper main-wrapper-1&quot;]/div[@class=&quot;main-content&quot;]/section[3]/form[@class=&quot;needs-validation&quot;]/div[@class=&quot;card&quot;]/div[@class=&quot;card-body&quot;]/div[@class=&quot;col-md-12 col-lg-12 p-0&quot;]/div[@class=&quot;form-group row&quot;]/div[@class=&quot;col-md-3 col-lg-3 mb-4&quot;]/select[@class=&quot;form-control&quot;]</value>
      <webElementGuid>80885e80-69cb-4105-a5b3-c6335c81738f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@name='angkatan']</value>
      <webElementGuid>cb6f09c8-3cd9-4654-bd89-efd2bfe10a49</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/div[3]/section[3]/form/div/div[2]/div/div/div[10]/select</value>
      <webElementGuid>d02cc58c-c8dd-45a7-a628-429acb4f78e9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Angkatan'])[2]/following::select[1]</value>
      <webElementGuid>3e46ecee-774d-4e7c-a46d-4f7c3eb9e79e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Oh no! You forget to fill this field.'])[9]/following::select[1]</value>
      <webElementGuid>58ffbb8a-458b-4a4b-adb8-0b968df0ed47</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Oh no! You forget to fill this field.'])[10]/preceding::select[1]</value>
      <webElementGuid>428a3654-05bd-40ab-80f3-2dfb86328405</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Alamat Siswa'])[1]/preceding::select[1]</value>
      <webElementGuid>12eada14-b087-4fd1-9854-1930a6e47e54</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[10]/select</value>
      <webElementGuid>84227d74-a631-4e87-9622-ce603aa96a98</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@name = 'angkatan' and (text() = '
                                        
                                                                                    2023
                                                                            ' or . = '
                                        
                                                                                    2023
                                                                            ')]</value>
      <webElementGuid>37cd01d8-94a7-4a8c-a80b-86bfd7a44829</webElementGuid>
   </webElementXpaths>
</WebElementEntity>

<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_Siswa Lama                          _b657e9</name>
   <tag></tag>
   <elementGuidId>3b66d9cd-1000-44a8-a461-6d1058ab29e3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='statusMasuk']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#statusMasuk</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>a30b63d7-0c8b-49e5-b29c-d344e465adbd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control</value>
      <webElementGuid>44f925c6-f203-4b11-82b4-549eb8830ba1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>status_masuk</value>
      <webElementGuid>087073a1-e877-4286-b365-e7ebc60fa73f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>statusMasuk</value>
      <webElementGuid>43bc9700-74b4-4383-ab71-9f2a2743343c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onchange</name>
      <type>Main</type>
      <value>addNewSelect()</value>
      <webElementGuid>212bc7c4-8521-4cd7-be70-09f3f3e90b46</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                        
                                        Siswa Lama
                                        Siswa Baru
                                        Siswa Pindahan
                                    </value>
      <webElementGuid>94e4b37c-a3d5-4c5e-9957-1cffefa2f098</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;statusMasuk&quot;)</value>
      <webElementGuid>db4c72a9-74e8-44e3-99a8-632a72d08919</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='statusMasuk']</value>
      <webElementGuid>09c13437-eccc-4a46-9c05-680c0c28f1e3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/div[3]/section[3]/form/div/div[4]/div/div/div/select</value>
      <webElementGuid>b34073fb-5aa8-432c-b017-6b472abc983b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Status Penerimaan'])[1]/following::select[1]</value>
      <webElementGuid>d163957c-afbe-4013-a949-b91a5646e64d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Informasi Penerimaan'])[1]/following::select[1]</value>
      <webElementGuid>5e3b8165-333c-4ee5-9412-dc9df54e85a3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Oh no! You forget to fill this field.'])[12]/preceding::select[1]</value>
      <webElementGuid>12764c25-4687-4c61-8c82-23d5be643a69</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Informasi Data Orang Tua'])[1]/preceding::select[1]</value>
      <webElementGuid>5d3476c4-6f6a-4afb-9f63-62717977aa4d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div/div/div/select</value>
      <webElementGuid>fd13cd97-930e-409a-9226-a7dd0ac73bf7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@name = 'status_masuk' and @id = 'statusMasuk' and (text() = '
                                        
                                        Siswa Lama
                                        Siswa Baru
                                        Siswa Pindahan
                                    ' or . = '
                                        
                                        Siswa Lama
                                        Siswa Baru
                                        Siswa Pindahan
                                    ')]</value>
      <webElementGuid>f9c8b03b-31c3-42b5-92f8-6f007134987b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>

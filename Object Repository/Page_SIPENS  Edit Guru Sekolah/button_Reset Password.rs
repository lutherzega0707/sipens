<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Reset Password</name>
   <tag></tag>
   <elementGuidId>3b8a54c1-5a21-4a45-b0a3-ae010e204ed2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>button.btn.btn-danger</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='app']/div/div[3]/section[3]/div/div/div/div[3]/form/button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>69ffe921-533a-464e-97f8-053e5566b99e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-danger</value>
      <webElementGuid>12c70fc5-ef8d-497f-ae3f-2223be074adc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                        
                                        Reset Password
                                    </value>
      <webElementGuid>bb5a4325-638b-429a-9892-ea1f05defd5b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;main-wrapper main-wrapper-1&quot;]/div[@class=&quot;main-content&quot;]/section[3]/div[@class=&quot;row&quot;]/div[@class=&quot;col-lg-4&quot;]/div[@class=&quot;card&quot;]/div[@class=&quot;card-footer&quot;]/form[1]/button[@class=&quot;btn btn-danger&quot;]</value>
      <webElementGuid>962d8cff-6f34-4588-8dd2-f981e00d5fa3</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/div[3]/section[3]/div/div/div/div[3]/form/button</value>
      <webElementGuid>8a9848fa-41a3-491b-aa6e-5e9ed8cd0219</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Foto Diri'])[1]/following::button[2]</value>
      <webElementGuid>75a5ef81-5aee-44d2-a037-ff534ab18064</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Data Guru'])[2]/preceding::button[1]</value>
      <webElementGuid>32fc2d27-6f4f-4d97-aa9d-f2bb3545257e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='NIP'])[1]/preceding::button[1]</value>
      <webElementGuid>f828a5a8-06c3-48b7-9844-fcbc12cde54a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Reset Password']/parent::*</value>
      <webElementGuid>2c9e3fad-ac84-449d-b772-2c76b2540551</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//form/button</value>
      <webElementGuid>586ee5eb-35e5-4073-9cb8-6154f32e2a23</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[(text() = '
                                        
                                        Reset Password
                                    ' or . = '
                                        
                                        Reset Password
                                    ')]</value>
      <webElementGuid>23a773a8-b6a6-45f7-bb35-67ad74c7b826</webElementGuid>
   </webElementXpaths>
</WebElementEntity>

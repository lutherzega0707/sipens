<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Edit</name>
   <tag></tag>
   <elementGuidId>ad8b71cd-affb-405d-86aa-3913384b86ed</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//table[@id='table-1']/tbody/tr[2]/td[5]/a[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>d24593d5-0b1e-49c5-afb8-ed226c61f55f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-icon btn-info ml-1</value>
      <webElementGuid>fc917f92-e349-416f-b5d2-7d38a209623d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>https://sipens.com/admin/edit-guru/a4832429-36e1-43a7-b684-761d1512118c</value>
      <webElementGuid>3600afb0-4817-42f5-80a0-0359f4195699</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                        
                                                        Edit
                                                    </value>
      <webElementGuid>44dddef9-2456-4f74-93a8-edd3870191aa</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;table-1&quot;)/tbody[1]/tr[2]/td[5]/a[@class=&quot;btn btn-icon btn-info ml-1&quot;]</value>
      <webElementGuid>b1f2084e-72db-4928-bb0e-60a3b484853e</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//table[@id='table-1']/tbody/tr[2]/td[5]/a[2]</value>
      <webElementGuid>9ddd221c-ec16-4d14-b97c-612cb5430884</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='ASLIKHATIN, S.Pd.'])[1]/preceding::a[1]</value>
      <webElementGuid>b42067cd-90a4-45c5-9c13-48f2cd7394e5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Aktif'])[3]/preceding::a[1]</value>
      <webElementGuid>8a8bfc0f-7bae-4c1e-aa24-f7cc3545edca</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, 'https://sipens.com/admin/edit-guru/a4832429-36e1-43a7-b684-761d1512118c')]</value>
      <webElementGuid>f0bf36df-bba7-4e83-a76d-1b470f6217ad</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tr[2]/td[5]/a[2]</value>
      <webElementGuid>59c4b118-5322-4e6c-bd51-a75e8ec1111c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'https://sipens.com/admin/edit-guru/a4832429-36e1-43a7-b684-761d1512118c' and (text() = '
                                                        
                                                        Edit
                                                    ' or . = '
                                                        
                                                        Edit
                                                    ')]</value>
      <webElementGuid>cbf2ca45-f11b-4e9b-9067-1a6d231de010</webElementGuid>
   </webElementXpaths>
</WebElementEntity>

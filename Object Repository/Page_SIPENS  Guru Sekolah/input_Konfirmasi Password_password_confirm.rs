<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Konfirmasi Password_password_confirm</name>
   <tag></tag>
   <elementGuidId>36d67fba-7f58-4b81-b68b-99c808ae7c47</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>input[name=&quot;password_confirm&quot;]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@name='password_confirm']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>c11fcd49-d317-4a0f-ab8b-f90150c2f918</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control</value>
      <webElementGuid>0b0c2957-2c67-40ce-b993-556298627a96</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>password</value>
      <webElementGuid>7f020fdc-0491-4af4-a809-fb01511e53a6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>password_confirm</value>
      <webElementGuid>733e7ef9-ba0b-4ff5-9e83-d315c3494927</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;inputData&quot;)/div[@class=&quot;modal-dialog modal-lg&quot;]/div[@class=&quot;modal-content&quot;]/form[@class=&quot;needs-validation&quot;]/div[@class=&quot;modal-body&quot;]/div[@class=&quot;col-md-12 col-lg-12 p-0&quot;]/div[@class=&quot;form-group row&quot;]/div[@class=&quot;col-md-6 col-lg-6 mb-3&quot;]/input[@class=&quot;form-control&quot;]</value>
      <webElementGuid>509ca5e3-ff84-4d45-81f9-dfe639596dce</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@name='password_confirm']</value>
      <webElementGuid>e8a4ecfb-6a63-4b7b-bb0c-4a1c9c7df4c5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='inputData']/div/div/form/div/div[3]/div/div[16]/input</value>
      <webElementGuid>76d9e63e-477a-455a-a946-380349700568</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[16]/input</value>
      <webElementGuid>dc492286-e3df-44be-889c-08effd4d5e7b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'password' and @name = 'password_confirm']</value>
      <webElementGuid>a2e88a10-572b-4cf8-9c4d-b15d3565b02a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>

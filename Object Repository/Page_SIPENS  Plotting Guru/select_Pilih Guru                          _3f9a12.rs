<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_Pilih Guru                          _3f9a12</name>
   <tag></tag>
   <elementGuidId>0f9b2a6b-7cf1-4b93-9571-2af850acded5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#guru</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='guru']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>ea7c0d2f-9299-4374-a307-4aa4e58fee1d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>guru</value>
      <webElementGuid>0545933e-af23-439b-8314-bd4fcb352813</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control</value>
      <webElementGuid>20e13261-562a-474b-a7e5-c07ba5f09507</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>guru</value>
      <webElementGuid>e97bfcb0-35bf-4f4c-97d4-afdb3fceecf1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                    Pilih Guru
                                                                            AGUS TONI
                                                                            Anin
                                                                            ASLIKHATIN, S.Pd.
                                                                            CAROLINE  INDRAWATI, S.Pd.
                                                                            DESSY FITRIA, S.Pd.
                                                                            DIRGA ADITYA RUSMA H
                                                                            Dra. CHENI CHAENIDA M. AYU,M.Pd.
                                                                            Dra. MUJI  UTAMI, M.Pd.
                                                                            Drs. AGUS  GUNAWAN, M.Pd.
                                                                            Drs. AINUL  YAQIN
                                                                            Drs. SUPRIH  UTOMO
                                                                            DWI RATNA YUWITANINGSIH,S.Pd.
                                                                            F. IDA  KRISTIANI, S.Pd. MM.
                                                                            GESANG  SAPTONO, S.Pd.
                                                                            Hj. ARIF  WIJAYATI, M.Pd.
                                                                            Hj. MUFIDATIK, S.Pd.
                                                                            Hj. NUR  HAYATI, S.Pd.
                                                                            Hj. SULASIH, M.Pd.
                                                                            IKHSAN EFENDI, S.Pd.I.
                                                                            LIS  KURNIA, S.Psi. S.Pd.
                                                                            M. ALI  ERFAN, S.Pd. M.Pd.
                                                                            MAR'ATUL MUSLIMAH, S.Pd.
                                                                            NANANG  ROESWANTONO, S.Pd.MM.
                                                                            NURIYAWATI, S.Pd.
                                                                            NURUL  QOMARIYAH, S.Pd.
                                                                            NURUL  WAHYU  P, S.Pd.
                                                                            RADEN SUMRATUL HANDAYANI, ST.
                                                                            Shani Indira
                                                                            SITI  DJUWARIJAH, S.Pd.
                                                                            SITI  JAUHARIYAH, S.Pd.
                                                                            SITI  MARIYANI, S.Pd.
                                                                            SOPHA  OLIVIATIE, S.Pd.
                                                                            STEFKA PRAJNA PARAMITA, S.Pd.
                                                                            SUSI  IDA  WARDANI, M.Psi.
                                                                            SYLVI INDAH W, S.Kom. M.Pd.
                                                                            TITIK  MURTANTI, S.Pd.
                                                                            WIWIET  PURNOMO, M.Pd.
                                                                            ZUHRIYAH ROHMAWATI, S.Pd.
                                                                    </value>
      <webElementGuid>27b5afdc-25ba-436f-b2ba-68dc9657698b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;guru&quot;)</value>
      <webElementGuid>f84a0f74-28d5-4676-83e9-acf1631a65b8</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='guru']</value>
      <webElementGuid>91314d23-f61e-4bbd-8463-7f006d4c3bc2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='form-update']/div/div/div[3]/select</value>
      <webElementGuid>01c89e5a-1748-4f29-94b5-996fd4154a3c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Pilih Guru'])[3]/following::select[1]</value>
      <webElementGuid>b99f46c0-8d18-469c-bac6-66b4c3b6e575</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Tahun Pelajaran'])[5]/following::select[2]</value>
      <webElementGuid>f7b12955-6ccc-46b8-8ff2-fda00de4ee67</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Mata Pelajaran'])[4]/preceding::select[1]</value>
      <webElementGuid>35a8c166-7f3e-4648-934b-0c64ab582e3a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Pilih Kelas'])[3]/preceding::select[2]</value>
      <webElementGuid>fddce20a-acb0-4ed8-af99-fcaa9d2d96ac</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]/div/div/form/div/div/div[3]/select</value>
      <webElementGuid>9bb0163f-720a-4ff4-af62-9e85252b291b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@name = 'guru' and @id = 'guru' and (text() = concat(&quot;
                                    Pilih Guru
                                                                            AGUS TONI
                                                                            Anin
                                                                            ASLIKHATIN, S.Pd.
                                                                            CAROLINE  INDRAWATI, S.Pd.
                                                                            DESSY FITRIA, S.Pd.
                                                                            DIRGA ADITYA RUSMA H
                                                                            Dra. CHENI CHAENIDA M. AYU,M.Pd.
                                                                            Dra. MUJI  UTAMI, M.Pd.
                                                                            Drs. AGUS  GUNAWAN, M.Pd.
                                                                            Drs. AINUL  YAQIN
                                                                            Drs. SUPRIH  UTOMO
                                                                            DWI RATNA YUWITANINGSIH,S.Pd.
                                                                            F. IDA  KRISTIANI, S.Pd. MM.
                                                                            GESANG  SAPTONO, S.Pd.
                                                                            Hj. ARIF  WIJAYATI, M.Pd.
                                                                            Hj. MUFIDATIK, S.Pd.
                                                                            Hj. NUR  HAYATI, S.Pd.
                                                                            Hj. SULASIH, M.Pd.
                                                                            IKHSAN EFENDI, S.Pd.I.
                                                                            LIS  KURNIA, S.Psi. S.Pd.
                                                                            M. ALI  ERFAN, S.Pd. M.Pd.
                                                                            MAR&quot; , &quot;'&quot; , &quot;ATUL MUSLIMAH, S.Pd.
                                                                            NANANG  ROESWANTONO, S.Pd.MM.
                                                                            NURIYAWATI, S.Pd.
                                                                            NURUL  QOMARIYAH, S.Pd.
                                                                            NURUL  WAHYU  P, S.Pd.
                                                                            RADEN SUMRATUL HANDAYANI, ST.
                                                                            Shani Indira
                                                                            SITI  DJUWARIJAH, S.Pd.
                                                                            SITI  JAUHARIYAH, S.Pd.
                                                                            SITI  MARIYANI, S.Pd.
                                                                            SOPHA  OLIVIATIE, S.Pd.
                                                                            STEFKA PRAJNA PARAMITA, S.Pd.
                                                                            SUSI  IDA  WARDANI, M.Psi.
                                                                            SYLVI INDAH W, S.Kom. M.Pd.
                                                                            TITIK  MURTANTI, S.Pd.
                                                                            WIWIET  PURNOMO, M.Pd.
                                                                            ZUHRIYAH ROHMAWATI, S.Pd.
                                                                    &quot;) or . = concat(&quot;
                                    Pilih Guru
                                                                            AGUS TONI
                                                                            Anin
                                                                            ASLIKHATIN, S.Pd.
                                                                            CAROLINE  INDRAWATI, S.Pd.
                                                                            DESSY FITRIA, S.Pd.
                                                                            DIRGA ADITYA RUSMA H
                                                                            Dra. CHENI CHAENIDA M. AYU,M.Pd.
                                                                            Dra. MUJI  UTAMI, M.Pd.
                                                                            Drs. AGUS  GUNAWAN, M.Pd.
                                                                            Drs. AINUL  YAQIN
                                                                            Drs. SUPRIH  UTOMO
                                                                            DWI RATNA YUWITANINGSIH,S.Pd.
                                                                            F. IDA  KRISTIANI, S.Pd. MM.
                                                                            GESANG  SAPTONO, S.Pd.
                                                                            Hj. ARIF  WIJAYATI, M.Pd.
                                                                            Hj. MUFIDATIK, S.Pd.
                                                                            Hj. NUR  HAYATI, S.Pd.
                                                                            Hj. SULASIH, M.Pd.
                                                                            IKHSAN EFENDI, S.Pd.I.
                                                                            LIS  KURNIA, S.Psi. S.Pd.
                                                                            M. ALI  ERFAN, S.Pd. M.Pd.
                                                                            MAR&quot; , &quot;'&quot; , &quot;ATUL MUSLIMAH, S.Pd.
                                                                            NANANG  ROESWANTONO, S.Pd.MM.
                                                                            NURIYAWATI, S.Pd.
                                                                            NURUL  QOMARIYAH, S.Pd.
                                                                            NURUL  WAHYU  P, S.Pd.
                                                                            RADEN SUMRATUL HANDAYANI, ST.
                                                                            Shani Indira
                                                                            SITI  DJUWARIJAH, S.Pd.
                                                                            SITI  JAUHARIYAH, S.Pd.
                                                                            SITI  MARIYANI, S.Pd.
                                                                            SOPHA  OLIVIATIE, S.Pd.
                                                                            STEFKA PRAJNA PARAMITA, S.Pd.
                                                                            SUSI  IDA  WARDANI, M.Psi.
                                                                            SYLVI INDAH W, S.Kom. M.Pd.
                                                                            TITIK  MURTANTI, S.Pd.
                                                                            WIWIET  PURNOMO, M.Pd.
                                                                            ZUHRIYAH ROHMAWATI, S.Pd.
                                                                    &quot;))]</value>
      <webElementGuid>8f75adb1-7fb8-4e84-aa09-33f0a5c67291</webElementGuid>
   </webElementXpaths>
</WebElementEntity>

<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_Pilih Mapel                         _181816</name>
   <tag></tag>
   <elementGuidId>48be2c21-679e-4f18-a813-c583c796bc98</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#mapel</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='mapel']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>f4479018-34d1-477a-b31a-6486d0febe3a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>mapel</value>
      <webElementGuid>5cdaeffc-dfbe-4ec1-bfee-828307ea2947</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control</value>
      <webElementGuid>049bb132-9482-4abc-9967-eaeb06a0ccaf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>mapel</value>
      <webElementGuid>00649ec5-ddfb-4bfe-b2b1-8d710e224371</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                    Pilih Mapel
                                                                            Bahasa Inggris
                                                                            IPA Edited
                                                                            Matematika
                                                                            IPS
                                                                    </value>
      <webElementGuid>fc34f20c-0b67-4b62-a368-7cbc177f63fb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;mapel&quot;)</value>
      <webElementGuid>4c37bad8-530c-4175-bce6-6055a4f6aa45</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='mapel']</value>
      <webElementGuid>e85443ab-2855-48d9-bb68-dcd35f714e86</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='modalPlotting']/div/div/form/div/div/div[4]/select</value>
      <webElementGuid>7f4135a2-3216-4004-806b-eba199f5e19e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Mata Pelajaran'])[3]/following::select[1]</value>
      <webElementGuid>d28c0ae8-6116-4626-b04a-b909c2ed823f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Pilih Guru'])[1]/following::select[2]</value>
      <webElementGuid>8d69caba-0a18-4307-88b2-aa8e6928d962</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Tambah Kelas'])[1]/preceding::select[1]</value>
      <webElementGuid>34c619b8-60d9-401a-93b9-658e55fdedb2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Pilih Kelas'])[1]/preceding::select[1]</value>
      <webElementGuid>3b44ee11-80b3-4e0f-8360-3f32bb3be217</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/select</value>
      <webElementGuid>631e10d3-aebb-4b53-a867-19dec301ae8a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@name = 'mapel' and @id = 'mapel' and (text() = '
                                    Pilih Mapel
                                                                            Bahasa Inggris
                                                                            IPA Edited
                                                                            Matematika
                                                                            IPS
                                                                    ' or . = '
                                    Pilih Mapel
                                                                            Bahasa Inggris
                                                                            IPA Edited
                                                                            Matematika
                                                                            IPS
                                                                    ')]</value>
      <webElementGuid>69188515-574e-4591-9099-0aaa415be30e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>

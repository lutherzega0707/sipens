<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Input Plotting Siswa</name>
   <tag></tag>
   <elementGuidId>70bf5100-b19d-4501-bff0-6ecbce335e88</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>a.btn.btn-primary</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='app']/div/div[3]/section[3]/div/div[2]/div/div[2]/a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>b43b439b-6c1a-48a6-bd13-3e4182fa9459</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-primary</value>
      <webElementGuid>532f1e4b-bcde-4ccd-b417-d5944565f90b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>https://sipens.com/admin/create-plotting-siswa</value>
      <webElementGuid>591243ae-6a57-4631-9cfe-28d3b42d104e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                
                                Input Plotting Siswa
                            </value>
      <webElementGuid>b001b3a6-67f9-4947-8d1a-a9741cb7f887</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;main-wrapper main-wrapper-1&quot;]/div[@class=&quot;main-content&quot;]/section[3]/div[@class=&quot;card&quot;]/div[@class=&quot;card-body&quot;]/div[@class=&quot;row mb-3&quot;]/div[@class=&quot;col-md-8 btn-plotting&quot;]/a[@class=&quot;btn btn-primary&quot;]</value>
      <webElementGuid>1732a57f-915a-41c5-ac3f-f2004ed85cc8</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/div[3]/section[3]/div/div[2]/div/div[2]/a</value>
      <webElementGuid>de5c353f-700d-4e7c-ba55-c4df9d36fb09</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Data Plotting Siswa'])[1]/following::a[1]</value>
      <webElementGuid>b6c4ee09-f7cb-403c-a150-c8ce15dbfc61</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Input Plotting Siswa'])[1]/following::a[1]</value>
      <webElementGuid>74720e79-9d1c-4a26-a2a2-67f6dd579cb1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='No'])[1]/preceding::a[1]</value>
      <webElementGuid>95124ae9-c355-4b14-a797-6c7b67e5c065</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Tahun Pelajaran'])[3]/preceding::a[1]</value>
      <webElementGuid>d04f9f2d-ad27-48a4-86e9-de4167236bde</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, 'https://sipens.com/admin/create-plotting-siswa')]</value>
      <webElementGuid>e6d20b9e-765c-483d-a069-2306b2d5f622</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div[2]/a</value>
      <webElementGuid>160442d5-486b-49c4-8b5c-93443f40824c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'https://sipens.com/admin/create-plotting-siswa' and (text() = '
                                
                                Input Plotting Siswa
                            ' or . = '
                                
                                Input Plotting Siswa
                            ')]</value>
      <webElementGuid>d09ce29a-993d-4db3-bdc1-63b0dae9568c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>

<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_7                                   _cb3210</name>
   <tag></tag>
   <elementGuidId>782cb9ee-7223-4cf4-a269-0b447ccd2f02</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#kelas</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='kelas']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>0db1803d-a911-412b-8735-bf83b26e3d6b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control</value>
      <webElementGuid>c4a019a0-9dd4-4865-b126-b7a270e92ba1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>kelas</value>
      <webElementGuid>5b085381-9157-435a-b6fd-929a55318153</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>kelas</value>
      <webElementGuid>7724aa27-305e-47b1-a9b7-ead948c330d1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onchange</name>
      <type>Main</type>
      <value>showSiswa()</value>
      <webElementGuid>ce5220b7-6aca-4532-a2a2-88d3131bc810</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                    
                                    7
                                    8
                                    9
                                </value>
      <webElementGuid>5049a43a-82f7-4ad0-b7d4-c8cfeeea9ffe</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;kelas&quot;)</value>
      <webElementGuid>891ced46-a3e1-4038-b5c8-f004cdb0cc11</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='kelas']</value>
      <webElementGuid>c1be128a-92db-46fc-9974-73d2cd402032</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/div[3]/section[3]/div/div[2]/form/div/div[2]/select</value>
      <webElementGuid>cc54874d-d68e-4d83-a6b8-6214079f9bd5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Kelas'])[1]/following::select[1]</value>
      <webElementGuid>06c3609f-ade0-45e3-8ba1-317b642d60bb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Oh no! You should fill this side.'])[1]/following::select[1]</value>
      <webElementGuid>90bc6ac0-52e2-49a2-aa5a-ad159baca752</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Good job!'])[2]/preceding::select[1]</value>
      <webElementGuid>f5017932-dcc7-46a1-a2c0-9148e733df0d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Oh no! You should fill this side.'])[2]/preceding::select[1]</value>
      <webElementGuid>28faa3ea-56a3-46f1-b9ca-8186fd13b519</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/select</value>
      <webElementGuid>13280ec0-0581-441c-8f70-76018f8083f3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@name = 'kelas' and @id = 'kelas' and (text() = '
                                    
                                    7
                                    8
                                    9
                                ' or . = '
                                    
                                    7
                                    8
                                    9
                                ')]</value>
      <webElementGuid>337a3d06-a9cf-430a-b997-6d14009b988f</webElementGuid>
   </webElementXpaths>
</WebElementEntity>

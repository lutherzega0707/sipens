<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Ubah Data Sekolah</name>
   <tag></tag>
   <elementGuidId>c0c78b14-8ba6-455e-a375-8b1f7e90ca3d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>button.btn.btn-success</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[@type='button']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>4e57178a-9965-4231-9e34-7b1803316612</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>a06bae21-8efe-485b-88bd-dc02bc4e9a0d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-success</value>
      <webElementGuid>27d76235-efb3-41d1-845b-5ad4919df5ad</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-toggle</name>
      <type>Main</type>
      <value>modal</value>
      <webElementGuid>fa5a603b-2082-40d4-8801-831296623c9d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-target</name>
      <type>Main</type>
      <value>#inputData</value>
      <webElementGuid>c7539b42-85d3-41de-98d2-88bd26dc5c59</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                        
                        Ubah Data Sekolah
                    </value>
      <webElementGuid>b97818bf-16ce-47a1-ba4d-d517ece9b9c6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;main-wrapper main-wrapper-1&quot;]/div[@class=&quot;main-content&quot;]/section[2]/div[@class=&quot;card col-lg-12 col-md-12&quot;]/div[@class=&quot;card-body p-0 pt-2 pb-4&quot;]/button[@class=&quot;btn btn-success&quot;]</value>
      <webElementGuid>adb6e88c-c57a-4a39-9b17-cb788fc20f73</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@type='button']</value>
      <webElementGuid>a11f619a-03a1-483c-9a14-2cae829bdd7f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/div[3]/section[2]/div/div[2]/button</value>
      <webElementGuid>6d0051d3-3829-4def-b5d6-edccc4daadad</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Informasi Pengguna'])[1]/following::button[1]</value>
      <webElementGuid>e4b9586d-865e-43a6-92fc-6ae33a7baaad</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Profil Sekolah'])[2]/following::button[1]</value>
      <webElementGuid>31f74822-cbe6-49dd-a5ef-2e26f1fb94da</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Logo Sekolah'])[1]/preceding::button[1]</value>
      <webElementGuid>954d0b0c-025e-465d-8664-313a7bd0657a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Informasi Data Profil Sekolah'])[1]/preceding::button[1]</value>
      <webElementGuid>72832d4e-cd50-46da-bf41-593170db0b68</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Ubah Data Sekolah']/parent::*</value>
      <webElementGuid>0af5b223-32c8-460f-b6dc-fbc3a0c5cb5e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//button</value>
      <webElementGuid>df0f9564-bb01-4737-9e85-5a46e01b4732</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'button' and (text() = '
                        
                        Ubah Data Sekolah
                    ' or . = '
                        
                        Ubah Data Sekolah
                    ')]</value>
      <webElementGuid>28cf183d-f1ee-4f42-854c-3fee2fd0219c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>

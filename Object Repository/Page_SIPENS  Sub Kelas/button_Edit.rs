<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Edit</name>
   <tag></tag>
   <elementGuidId>fe3a6016-fe85-4d46-9ba0-85350626e012</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//button[@value='7']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>7e51dda7-a925-4c87-9fe4-328d4a97efa5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-info open_modal</value>
      <webElementGuid>5cb9af06-f5a5-4571-bc39-a94c58bffa56</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>7</value>
      <webElementGuid>2516abb6-4e54-45f7-bc6b-beed3e49ceda</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                
                                                Edit
                                            </value>
      <webElementGuid>4b299564-810d-47dc-a5cd-99401f73bd71</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;table-2&quot;)/tbody[1]/tr[@class=&quot;text-center&quot;]/td[4]/button[@class=&quot;btn btn-info open_modal&quot;]</value>
      <webElementGuid>64a00aba-5411-4657-9c43-01cfc91fc550</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@value='7']</value>
      <webElementGuid>88458dd8-d1de-466e-a63d-47cd2be1a1ab</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//table[@id='table-2']/tbody/tr[7]/td[4]/button</value>
      <webElementGuid>9bde7d6a-bc33-4ae4-a2d7-a556424d59a1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='G'])[1]/following::button[1]</value>
      <webElementGuid>f16c8ebf-9596-4291-8336-896159d9f178</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Copyright © 2023 SIPENS'])[1]/preceding::button[1]</value>
      <webElementGuid>5a230705-59b7-4267-b1bb-8fd6eb527a04</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Input Sub Kelas'])[2]/preceding::button[1]</value>
      <webElementGuid>f8426400-7c99-4b15-a89f-f38faa9176f9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tr[7]/td[4]/button</value>
      <webElementGuid>11c539e5-0342-4c45-9e1d-c9cbe92f8868</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[(text() = '
                                                
                                                Edit
                                            ' or . = '
                                                
                                                Edit
                                            ')]</value>
      <webElementGuid>9687fff8-88e0-4c4e-9747-bd4899e77dd4</webElementGuid>
   </webElementXpaths>
</WebElementEntity>

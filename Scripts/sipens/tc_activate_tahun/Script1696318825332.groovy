import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://sipens.com/login')

WebUI.click(findTestObject('Object Repository/Page_Login  SIPENS/input_Username_username'))

WebUI.setText(findTestObject('Object Repository/Page_Login  SIPENS/input_Username_username'), '20500492')

WebUI.setEncryptedText(findTestObject('Object Repository/Page_Login  SIPENS/input_Forgot Password_password'), 'LjW/QG7ZUgIynvlB3Cr/Vw==')

WebUI.click(findTestObject('Object Repository/Page_Login  SIPENS/button_Login'))

WebUI.click(findTestObject('Object Repository/Page_SIPENS  Dashboard/button_OK'))

WebUI.click(findTestObject('Object Repository/Page_SIPENS  Dashboard/span_Tahun Pelajaran'))

WebUI.click(findTestObject('Object Repository/Page_SIPENS  Tahun Pelajaran/button_Ubah Tahun Aktif'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Page_SIPENS  Tahun Pelajaran/select_Ganjil                                Genap'), 
    '1', true)

WebUI.click(findTestObject('Object Repository/Page_SIPENS  Tahun Pelajaran/button_Ubah Tahun Aktif_1'))

WebUI.verifyElementText(findTestObject('Object Repository/Page_SIPENS  Tahun Pelajaran/div_Ubah Tahun Pelajaran Aktif Berhasil'), 
    'Ubah Tahun Pelajaran Aktif Berhasil!')

WebUI.click(findTestObject('Object Repository/Page_SIPENS  Tahun Pelajaran/button_OK'))
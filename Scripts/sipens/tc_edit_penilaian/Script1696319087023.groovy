import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://sipens.com/login')

WebUI.setText(findTestObject('Object Repository/Page_Login  SIPENS/input_Username_username'), '199308252022211001')

WebUI.setEncryptedText(findTestObject('Object Repository/Page_Login  SIPENS/input_Forgot Password_password'), 'Kuta7+MTGZaWMJVkBfoNgXB8OP787BF7')

WebUI.click(findTestObject('Object Repository/Page_Login  SIPENS/button_Login'))

WebUI.click(findTestObject('Object Repository/Page_SIPENS  Dashboard/button_OK'))

WebUI.click(findTestObject('Object Repository/Page_SIPENS  Dashboard/span_Ubah Akses'))

WebUI.click(findTestObject('Object Repository/Page_SIPENS  Dashboard/a_Guru Mapel'))

WebUI.click(findTestObject('Object Repository/Page_SIPENS  Dashboard/button_OK'))

WebUI.click(findTestObject('Object Repository/Page_SIPENS  Dashboard/span_Penilaian'))

WebUI.click(findTestObject('Object Repository/Page_SIPENS  Penilaian/div_Informasi Pengguna_accordion-header'))

WebUI.click(findTestObject('Object Repository/Page_SIPENS  Penilaian/a_kolaborasi'))

WebUI.click(findTestObject('Object Repository/Page_SIPENS  Penilaian/a_Kerjasama'))

WebUI.delay(10)

WebUI.click(findTestObject('Object Repository/Page_SIPENS  Penilaian/button_'))

WebUI.click(findTestObject('Object Repository/Page_SIPENS  Penilaian/input_Kerjasama_header-checkbox'))

WebUI.click(findTestObject('Object Repository/Page_SIPENS  Penilaian/button_Simpan Nilai'))

WebUI.verifyElementText(findTestObject('Object Repository/Page_SIPENS  Penilaian/div_Input Nilai Siswa Berhasil'), 'Input Nilai Siswa Berhasil.')

WebUI.click(findTestObject('Object Repository/Page_SIPENS  Penilaian/button_OK'))